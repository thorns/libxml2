#! /bin/sh
# /*@@
#  @file   libxml2.sh
#  @date   2009-06-12
#  @author Erik Schnetter <schnetter@cct.lsu.edu.de>
#  @desc
#          Setup libxml2 as a thorn
#  @enddesc
# @@*/



# /*@@
#   @routine    CCTK_Search
#   @date       Wed Jul 21 11:16:35 1999
#   @author     Tom Goodale
#   @desc
#   Used to search for something in various directories
#   @enddesc
#@@*/

CCTK_Search()
{
  eval  $1=""
  if test $# -lt 4 ; then
    cctk_basedir=""
  else
    cctk_basedir="$4/"
  fi
  for cctk_place in $2
    do
#      echo $ac_n "  Looking in $cctk_place""...$ac_c" #1>&6
      if test -r "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
      if test -d "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
#      echo "$ac_t"" No" #1>&6
    done

  return
}


# Work out where libxml2 is installed
if [ -z "$LIBXML2_DIR" ]; then
  echo "BEGIN MESSAGE"
  echo 'LIBXML2 selected but no LIBXML2_DIR set.  Checking some places...'
  echo "END MESSAGE"

  CCTK_Search LIBXML2_DIR "/usr /usr/local /usr/local/libxml2 /usr/local/packages/libxml2 /usr/local/apps/libxml2 $HOME c:/packages/libxml2" bin/xml2-config
  if [ -z "$LIBXML2_DIR" ]; then
     echo "BEGIN ERROR" 
     echo 'Unable to locate the libxml2 directory -- please set LIBXML2_DIR'
     echo "END ERROR" 
     exit 2
  fi
  echo "BEGIN MESSAGE"
  echo "Found a libxml2 package in $LIBXML2_DIR"
  echo "END MESSAGE"
fi



LIBXML2_INC_DIRS="$(${LIBXML2_DIR}/bin/xml2-config --cflags | sed -e 's/^-I//g;s/ -I/ /g')"
LIBXML2_LIB_DIRS="$(${LIBXML2_DIR}/bin/xml2-config --libs   | sed -e 's/^-L//g;s/ -L/ /g')"
LIBXML2_LIBS=''

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_LIBXML2     = 1"
echo "LIBXML2_DIR      = ${LIBXML2_DIR}"
echo "LIBXML2_INC_DIRS = ${LIBXML2_INC_DIRS}"
echo "LIBXML2_LIB_DIRS = ${LIBXML2_LIB_DIRS}"
echo "LIBXML2_LIBS     = ${LIBXML2_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(LIBXML2_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(LIBXML2_LIB_DIRS)'
echo 'LIBRARY           $(LIBXML2_LIBS)'
